
import SpriteKit

import GameplayKit



class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var gameLevelWave = 1
    
    var screenHeight:CGFloat = 0
    var screenWidth:CGFloat = 0
    
    var cat:SKSpriteNode = SKSpriteNode()
    var rats:SKSpriteNode = SKSpriteNode()
    var cheese:SKSpriteNode = SKSpriteNode()
    var ratSprites: [SKSpriteNode] = []
    
    //to draw lives circle
    var circle1 = SKShapeNode(circleOfRadius: 20)
    var circle2 = SKShapeNode(circleOfRadius: 20)
    
    var cheeseLoc:CGPoint = CGPoint()
    
    var score = 0
    var lives = 35
    
    // MARK: Label variables
    var livesLabel:SKLabelNode = SKLabelNode(text:"")
    var scoreLabel:SKLabelNode = SKLabelNode(text:"")
    var restartLabel:SKLabelNode = SKLabelNode(text:"Restart")
  
    //-Game Testing
    
    var pathArray = [CGPoint]()
    
    //Gesture Variable
    var tapGesture = UITapGestureRecognizer()
    var tapGestureChar = ""
    
    override func didMove(to view: SKView) {
        
        let playSoundAction = SKAction.playSoundFileNamed("BackgroundMusic/SaveTheCheese",waitForCompletion: true)
        let play = SKAction.repeatForever(playSoundAction)
        //self.run(play)
        
        // add a boundary around the scene
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        self.physicsWorld.contactDelegate = self
        
        
        //Left swipe gesture
        let LswipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(LRswiped(_gesture:)))
        LswipeGesture.direction = .left
        self.view?.addGestureRecognizer(LswipeGesture)
        
        //Right swipe gesture
        let RswipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(LRswiped(_gesture:)))
        RswipeGesture.direction = .right
        self.view?.addGestureRecognizer(RswipeGesture)
        
        //Up swipe gesture
        let UswipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(UDswiped(_gesture:)))
        UswipeGesture.direction = .up
        self.view?.addGestureRecognizer(UswipeGesture)
        
        //Down swipe gesture
        let DswipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(UDswiped(_gesture:)))
        DswipeGesture.direction = .down
        self.view?.addGestureRecognizer(DswipeGesture)
        
        //Double tap Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(DTapped(_gesture:)))
        tapGesture.numberOfTapsRequired = 2
        self.view?.addGestureRecognizer(tapGesture)
        
        
        //Single tap Gesture
        let SingleTapGesture = UITapGestureRecognizer(target: self, action: #selector(STapped(_gesture:)))
        SingleTapGesture.numberOfTapsRequired = 1
        self.view?.addGestureRecognizer(SingleTapGesture)
        
        
        self.cat = self.childNode(withName: "cat") as! SKSpriteNode
//        self.rats = self.childNode(withName: "rats") as! SKSpriteNode
        self.cheese = self.childNode(withName: "cheese") as! SKSpriteNode
        
        self.scoreLabel = self.childNode(withName: "scoreLable") as! SKLabelNode
        self.livesLabel = self.childNode(withName: "livesLable") as! SKLabelNode
        self.restartLabel = self.childNode(withName: "restartLabel") as! SKLabelNode
        
        scoreLabel.text = "Score: \(self.score)"
        livesLabel.text = "Lives: \(self.lives)"
        
        self.cheeseLoc = self.cheese.position
        
        self.screenHeight = self.size.height
        self.screenWidth = self.size.width
        
        
        /*
 
         Physics rules
         static let None:  UInt32 = 0
         1 - static let Cheese:   UInt32 = 0b1      // 0x00000001 = 1
         2 - static let Rats: UInt32 = 0b10     // 0x00000010 = 2
         4 - static let Cat:   UInt32 = 0b100    // 0x00000100 = 4
 
        */
        
        self.cat.physicsBody!.categoryBitMask = PhysicsCategory.Cat
        //self.rats.physicsBody!.categoryBitMask = PhysicsCategory.Rats
        self.cheese.physicsBody!.categoryBitMask = PhysicsCategory.Cheese
        
       
        self.cheese.physicsBody?.contactTestBitMask = 2
        
        getSomeRats()
        
        //to call the life circle
        //drawLivesCircle()
    }
    
    
    @objc func LRswiped(_gesture:UISwipeGestureRecognizer)
    {
        print("Horizontal Swipe")
        self.tapGestureChar = "-"
    }
    
  
    @objc func UDswiped(_gesture:UISwipeGestureRecognizer)
    {
        print("Vertical Swipe")
        self.tapGestureChar = "|"
    }
  
    @objc func DTapped(_gesture:UISwipeGestureRecognizer)
    {
        print("Double Tap")
        self.getMoreLives()
        self.tapGestureChar = ".."
    }
    
    @objc func STapped(_gesture:UISwipeGestureRecognizer)
    {
        print("Single Tap")
        self.tapGestureChar = "."
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    
        //@TODO: make random rats
        //self.addChild(spawnRats())
        
        //@TODO: move rats towards
        
        runTheRats()
        
        //to draw life circle
        /*
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { // Change `2.0` to the desired number of seconds.
            self.drawLivesCircle()
        }
 */
        
    }
    
   
    func didBegin(_ contact: SKPhysicsContact) {
        
        
        let playEatCheese = SKAction.playSoundFileNamed("BackgroundMusic/cheese_eat",waitForCompletion: false)
        self.run(playEatCheese)
        
        
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        //print("Collision: \(nodeA?.name!) hit \(nodeB?.name!)")
     
        if (nodeB?.name! == "rats") {
        
            nodeB?.removeFromParent()
            updateLives()
            
        }
        
        
    }
    
    func drawLivesCircle(){
        //draw a circle on mouse
        self.circle1.strokeColor = SKColor.yellow
        // width of border
        self.circle1.lineWidth = 5
        
        // location of circle
        self.circle1.position = CGPoint(x:400, y:100)
        //draw a circle on mouse
        self.circle2.strokeColor = SKColor.yellow
        // width of border
        self.circle2.lineWidth = 5
        
        // location of circle
        self.circle2.position = CGPoint(x:445, y:100)
        addChild(circle1)
        addChild(circle2)
    }
    
    func getMoreLives()
    {
            self.lives = self.lives + 1
            livesLabel.text = "Lives: \(self.lives)"
            
    }
    
    
    func updateLives()
    {
        if (self.lives == 1) {
            //end game
            endGame()
        }
        else {
            self.lives = self.lives - 1
            livesLabel.text = "Lives: \(self.lives)"
            
        }
    }
    
    func endGame() {
     
        
            // increase the level number
            let message = SKLabelNode(text:"Game Over!")
            message.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
            message.fontColor = UIColor.white
            message.fontSize = 110
            message.fontName = "AvenirNext-Bold"
            
            addChild(message)
            
            // load the next level
            self.gameLevelWave = 1
            
            // try to load the next level
            guard let nextLevelScene = GameScene.loadLevel(levelNumber: 1) else {
                print("Error when loading next level")
                return
            }
            
            // wait 1 second then switch to next leevl
            let waitAction = SKAction.wait(forDuration:1)
            
            let showNextLevelAction = SKAction.run {
                let transition = SKTransition.flipVertical(withDuration: 2)
                nextLevelScene.scaleMode = .aspectFill
                self.scene?.view?.presentScene(nextLevelScene, transition:transition)
            }
            
            let sequence = SKAction.sequence([waitAction, showNextLevelAction])
            
            self.run(sequence)
        
    }

    class func loadLevel(levelNumber:Int) -> GameScene? {
        let fileName = "GameScene"
        
        // DEBUG nonsense
        print("Trying to load file: GameScene.sks")
        
        guard let scene = GameScene(fileNamed: fileName) else {
            print("Cannot find level named: GameScene.sks")
            return nil
        }
        
        scene.scaleMode = .aspectFill
        return scene
        
    }
    
    func touchDown(atPoint pos : CGPoint)
    {
    
        pathArray.removeAll()
        pathArray.append(pos)
    }
    
    func touchMoved(atPoint pos : CGPoint)
    {
        pathArray.append(pos)
        createLine()
    }
    
    func touchUp(atPoint pos : CGPoint)
    {
        print(pathArray.count)
        
    }
    
    func createLine()
    {
        let path = CGMutablePath()
        path.move(to: pathArray[0])
        for point in pathArray
        {
            path.addLine(to: point)
        }

        let line = SKShapeNode()
        line.path = path
        line.fillColor = .clear
        line.lineWidth = 20
        line.strokeColor = .red
        line.lineCap = .round
        
        self.addChild(line)
        let fade:SKAction = SKAction.fadeOut(withDuration: 0.5)
        fade.timingMode = .easeIn
        let remove:SKAction = SKAction.removeFromParent()
        line.run(SKAction.sequence([fade, remove]))
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches{
        self.touchDown(atPoint: t.location(in: self))
        }
        
        guard let touch = touches.first  else {
            return
        }
        
        let mouseLocation = touch.location(in: self)
        //print("Finger starting position: \(mouseLocation)")
        
        // detect what sprite was touched
        let spriteTouched = self.atPoint(mouseLocation)
        
        
        
        if (spriteTouched.name == "restartLabel") {
          self.restartGame()
            
        }
  }

    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {

        for t in touches{
        
            self.touchMoved(atPoint: t.location(in: self))
        
        }

    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {

        for t in touches{
            
            self.touchUp(atPoint: t.location(in: self))
            
        }
        
    }
    
    
    func spawnRats() -> SKSpriteNode {
        
        // create a new enemy sprite
        let newRat = RatNew(imageNamed:"mouse")
        newRat.name = "rats"
        newRat.xScale = 0.60
        newRat.yScale = 0.60
        newRat.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:newRat.frame.width,height:newRat.frame.height))
        newRat.physicsBody?.affectedByGravity = false
        newRat.physicsBody?.isDynamic = false
        
        
        let xMin = (Int) (self.screenWidth - (self.screenWidth * 2))
        let xMax = (Int) (self.screenWidth)
        
        let yMin = -225
        let yMax = (Int) ((self.screenHeight - self.screenHeight * 2) / 2)
        
        let randomX = Int.random(in: xMin..<xMax)
        let randomY = Int.random(in: yMax..<yMin)
        
        print("Screen Height: \(self.screenHeight) RandomY: \(randomY) Screen Width: \(self.screenWidth) RandomX: \(randomX)")
        if (randomX < 0 ) {
             newRat.xScale = newRat.xScale * -1;
        }
        newRat.position = CGPoint(x: CGFloat(randomX), y: CGFloat(randomY))
    
        // add pattern to rat
        
        // Generate random number between 1 to 3
        // 1 - Horz line, 2 - vert line, 3 - circle (single tap)

        self.gameLevelWave = 3
        
        // get the game level
        
        if (self.gameLevelWave == 1) {
           // newRat.generateOneRandomPattern()
           // newRat.addPattern()
            newRat.drawOnePattern()
        }
        else if (self.gameLevelWave == 2) {
            // newRat.generateTwoRandomPattern()
            newRat.drawTwoPattern()
        }
        else if (self.gameLevelWave == 3) {
            // newRat.generateThreeRandomPattern()
            newRat.drawThreePattern()
        }
        // draw pattern accroding to the game level
        
        // add rat to the sprite list
        ratSprites.append(newRat)
        return newRat
    }
    
    
    func restartGame () {
        let scene = GameScene(fileNamed: "GameScene")
        self.gameLevelWave = 0
        self.lives = 35
        self.score = 0
        scene!.scaleMode = scaleMode
        view!.presentScene(scene)
    }
    
    
    func runTheRats() {
        if ( self.childNode(withName: "rats") != nil) {
            for node in self.children {
                if ( node.name == "rats") {
                    //print ("Enemy Move to moveToX: \(moveToX) + moveToY: \(moveToY)")
                    
                    let moveToPlayer = SKAction.move(to: cheeseLoc, duration: 10)
                    node.run(moveToPlayer)
                }
                else {
                   // getSomeRats()
                   // self.endGame()
                }
            }
        }
    }
    
    
    func getSomeRats() {
        
        
       // self.gameLevelWave = self.gameLevelWave + 1
        
        print("Game Level: \(self.gameLevelWave)")
        
        var run = 10 * self.gameLevelWave
        
        while(true)
        {
            self.addChild(spawnRats())
            //Thread.sleep(until: Date(timeIntervalSinceNow: 0.25))
            run = run - 1
            if(run == 0) {
                break
            }
        }
        
    }
    
    
    struct PhysicsCategory {
        static let None:  UInt32 = 0
        static let Cheese:   UInt32 = 0b1      // 0x00000001 = 1
        static let Rats: UInt32 = 0b10     // 0x00000010 = 2
        static let Cat:   UInt32 = 0b100    // 0x00000100 = 4
       }
}
