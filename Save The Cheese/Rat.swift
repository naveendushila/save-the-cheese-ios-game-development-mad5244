//
//  Rat.swift
//  Save The Cheese
//
//  Created by MacStudent on 2019-02-28.
//  Copyright © 2019 Graphisigner. All rights reserved.
//


import SpriteKit

class Rat:SKSpriteNode {
    
    
    var circle = SKShapeNode(circleOfRadius: 10)
    var verticalLine = SKShapeNode()
    var pathToVerticalLine = CGMutablePath()
    
    var horizontalLine = SKShapeNode()
    var pathToHorizontalLine = CGMutablePath()
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        // default constructor
        //let texture = SKTexture(imageNamed: "mouse")
        //let size = texture.size()
        let color = UIColor.clear
        
        super.init(texture: texture, color: color, size: size)
        
        //physicsBody = SKPhysicsBody(circleOfRadius: size.width / 2)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addPattern() {
        self.drawVerticalLine()
        self.drawSingleCircle()
        self.drawHorizontalLine()
    }
    
    func drawVerticalLine()
    {
        self.pathToVerticalLine.move(to: CGPoint(x: 100.0, y: 100.0))
        self.pathToVerticalLine.addLine(to: CGPoint(x: 100.0, y: 50.0))
        self.verticalLine.path = self.pathToVerticalLine
        self.verticalLine.strokeColor = SKColor.red
        self.verticalLine.lineWidth = 5
        self.verticalLine.position = CGPoint(x:-40, y:0)
        addChild(self.verticalLine)
    }
    
    func drawHorizontalLine()
    {
        self.pathToHorizontalLine.move(to: CGPoint(x: 100, y: 75))
        self.pathToHorizontalLine.addLine(to: CGPoint(x: 150, y: 75))
        self.horizontalLine.path = self.pathToHorizontalLine
        self.horizontalLine.strokeColor = SKColor.green
        self.horizontalLine.lineWidth = 5
        addChild(self.horizontalLine)
    }
    
    func drawSingleCircle(){
        //draw a circle on mouse
        self.circle.strokeColor = SKColor.magenta
        // width of border
        self.circle.lineWidth = 5
        // fill color
        self.circle.fillColor = SKColor.magenta
        // location of circle
        self.circle.position = CGPoint(x:0, y:self.size.height)
        
        addChild(self.circle)
        
//        print("rat position: \(self.position)")
//        print("circle position: \(self.circle.position)")
//        print("hor position: \(self.horizontalLine.position)")
//        print("ver position: \(self.verticalLine.position)")
//        print("====")
    }
    
    func removeSingleCircle() {
        self.circle.removeFromParent()
    }
    
    func removeHorizontalLine() {
        self.horizontalLine.removeFromParent()
    }
    
    func removeVerticalLine() {
        self.verticalLine.removeFromParent()
    }
    
    
    // 1 - Horz line, 2 - vert line, 3 - circle (single tap)
    
    
    func generateOneRandomPattern() {
        let randomPattern1 = Int.random(in: 1...3)
       self.giveRandomPattern(patternCode: randomPattern1)
    
    }
    
    func generateTwoRandomPattern() {
        let randomPattern1 = Int.random(in: 1...3)
        let randomPattern2 = Int.random(in: 1...3)
        self.giveRandomPattern(patternCode: randomPattern1)
        self.giveRandomPattern(patternCode: randomPattern2)
    }
    
    func generateThreeRandomPattern () {
        let randomPattern1 = Int.random(in: 1...3)
        let randomPattern2 = Int.random(in: 1...3)
        let randomPattern3 = Int.random(in: 1...3)
        self.giveRandomPattern(patternCode: randomPattern1)
        self.giveRandomPattern(patternCode: randomPattern2)
        self.giveRandomPattern(patternCode: randomPattern3)
    }
    
    func giveRandomPattern( patternCode:Int) {
        if (patternCode == 1 ) {
            self.drawHorizontalLine()
        }
        else if (patternCode == 2) {
            self.drawVerticalLine()
        }
        else if (patternCode == 3) {
            self.drawSingleCircle()
        }
    }
}
