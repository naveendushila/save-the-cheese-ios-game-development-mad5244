//
//  RatNew.swift
//  Save The Cheese
//
//  Created by Naveen Dushila on 2019-02-28.
//  Copyright © 2019 Graphisigner. All rights reserved.
//

import SpriteKit

class RatNew:SKSpriteNode {
    
    let charArrays:[Character] = ["-","|","."]
    
    var randomChar = ""
    
    var ratLabel:SKLabelNode = SKLabelNode()
    
    var tempLabel:[Character] = ["1","2","3","4","5"]
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        let color = UIColor.clear
        super.init(texture: texture, color: color, size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func drawOnePattern()
    {
       
        let randomPattern1 = Int.random(in: 0..<3)
        
        self.tempLabel[0] = self.charArrays[randomPattern1]
        
        ratLabel.text = String(self.tempLabel)
        ratLabel.position = CGPoint(x:0, y:100)
        ratLabel.fontColor = UIColor.red
        ratLabel.fontSize = 110
        ratLabel.fontName = "AvenirNext-Bold"
        
        addChild(self.ratLabel)
    }
    
    func drawTwoPattern()
    {
        let randomPattern1 = Int.random(in: 0..<3)
        let randomPattern2 = Int.random(in: 0..<3)
        
        self.tempLabel[0] = self.charArrays[randomPattern1]
        self.tempLabel[1] = " "
        self.tempLabel[2] = self.charArrays[randomPattern2]
        
        ratLabel.text = String(self.tempLabel)
        ratLabel.position = CGPoint(x:0, y:100)
        ratLabel.fontColor = UIColor.red
        ratLabel.fontSize = 110
        ratLabel.fontName = "AvenirNext-Bold"
        
        addChild(self.ratLabel)
    }
    
    func drawThreePattern()
    {
        let randomPattern1 = Int.random(in: 0..<3)
        let randomPattern2 = Int.random(in: 0..<3)
        let randomPattern3 = Int.random(in: 0..<3)
        
        
        self.tempLabel[0] = self.charArrays[randomPattern1]
        self.tempLabel[1] = " "
        self.tempLabel[2] = self.charArrays[randomPattern2]
        self.tempLabel[3] = " "
        self.tempLabel[4] = self.charArrays[randomPattern3]
        
        ratLabel.text = String(self.tempLabel)
        ratLabel.position = CGPoint(x:0, y:100)
        ratLabel.fontColor = UIColor.red
        ratLabel.fontSize = 110
        ratLabel.fontName = "AvenirNext-Bold"
        
        addChild(self.ratLabel)
    }

    func compareGesture(gesture: Character) {
    
        var index = 0
        while(index < 4) {
            if(self.charArrays[index] == gesture)
            {
                removeChar(charIndex: index)
            }
            else{
                continue
            }
            index = index - 1
        }
        
    }
    
    func getString () -> [Character] {
        return self.charArrays
    }
    
    func removeChar( charIndex: Int) {
        
        self.tempLabel[charIndex] = " "
        
        self.ratLabel.text = String(self.tempLabel)
        
    }
    
}
